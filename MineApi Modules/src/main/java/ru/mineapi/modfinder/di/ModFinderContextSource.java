package ru.mineapi.modfinder.di;

import ru.mineapi.di.MineApiContext;
import ru.mineapi.modfinder.ModFinder;
import ru.mineapi.modfinder.classfinders.CachedModClassFinder;
import ru.mineapi.modfinder.classfinders.ScannerModsClassFinder;
import ru.mineapi.modfinder.classfinders.cached.ModsClassesCache;
import ru.mineapi.modfinder.config.ModFinderConfig;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.ioc.DIManager;
import ru.swayfarer.swl2.ioc.DIManager.DISwL;
import ru.swayfarer.swl2.ioc.DIRegistry;
import ru.swayfarer.swl2.ioc.componentscan.DISwlSource;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.observable.property.ObservableProperty;
import ru.swayfarer.swl2.resource.file.FileSWL;

/**
 * Источник контекста загрузчика jar-файлов <br>
 * 
 * @author swayfarer
 *
 */
@SuppressWarnings("rawtypes")
@DISwlSource(context = ModFinderContextSource.contextName)
public class ModFinderContextSource {

	/** Имя контекста этого модуля */
	@InternalElement
	public static final String contextName = "mineapi|modfinder";
	
	/**
	 * Метод выполнится до доабвления элемента в контекст. <br>
	 * Тут создается и настраивается сам контекст (например, в паренты добавляется стандартный (core) контекст mineapi
	 */
	@DISwlSource.PreInitEvent
	public static void initContext()
	{
		DIManager.createIfNotFound(contextName);
		DIRegistry.addContextParent(contextName, MineApiContext.contextName);
		DIRegistry.registerContextPattern(contextName, ModFinder.class.getPackage().getName());
	}
	
	/**
	 * Конфигурация загрузчика jar-файлов из папки модов
	 * @param modFinderConfigPath Значение контекста, которое указывает расположение конфигурации 
	 * @return Конфигурация
	 */
	@DISwL
	public ModFinderConfig modFinderConfig(ObservableProperty<String> modFinderConfigPath)
	{
		ModFinderConfig config = new ModFinderConfig();
		
		config.setResourceLink(modFinderConfigPath.get());
		config.listen(500);
		
		config.createIfNotFound();
		config.init();
		
		return config;
	}
	
	/**
	 * Кэш сканнера модов внутри папки модов. <br>
	 * Используется для ускорения их загрузки путем переиспользования части результатов старого сканирования
	 * @param mineApiCache Директория кэша MineApi (из контекста, этого, или родительских)
	 * @return Кэш
	 */
	@DISwL
	public ModsClassesCache modsClassesCache(ObservableProperty<FileSWL> mineApiCacheDir)
	{
		ModsClassesCache config = new ModsClassesCache();
		
		config.setResourceLink(mineApiCacheDir.get().subFile("modsCache.lua").toRlink());
		config.listen(500);
		
		config.createIfNotFound();
		config.init();
		
		return config;
	}
	
	/**
	 * Функция, возвращающая список классов модов внутри jar-файла <br>
	 * Используется для гибкой настройки способа сканирования директории модов, например, через нее переключаются использование кэша и его не использование. 
	 * @param modFinderConfig Конфигурация загрузчика, см {@link #modFinderConfig(ObservableProperty)} 
	 * @return Функция
	 */
	@DISwL
	public IFunction1 fileToModsClassesFun(ModFinderConfig modFinderConfig)
	{
		return modFinderConfig.isUsingCache.get() ? new CachedModClassFinder() : new ScannerModsClassFinder();
	}
}
