package ru.mineapi.modfinder.config;

import ru.mineapi.modfinder.classfinders.CachedModClassFinder;
import ru.mineapi.modfinder.classfinders.ScannerModsClassFinder;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.observable.property.ObservableProperty;
import ru.swayfarer.swl2.swconf.config.AutoSerializableConfig;
import ru.swayfarer.swl2.swconf.serialization.comments.CommentSwconf;

/**
 * Конфигурация загрузчика jar-файлов в директории модов
 * @author swayfarer
 *
 */
public class ModFinderConfig extends AutoSerializableConfig {

	/** Использоваь ли кэширование при сканировании модов? ({@link CachedModClassFinder} вместо {@link ScannerModsClassFinder})*/
	@CommentSwconf("If sets to true mineapi will use caching for boost mods scanning ")
	public ObservableProperty<Boolean> isUsingCache = Observables.createProperty(true);
	
	/** Расшрирение файлов, которые будут сканироваться на наличие в них модов внутри папки модов */
	@CommentSwconf("Extension of mod-files in directory of mods ")
	public ObservableProperty<String> modFileExtension = Observables.createProperty("mineapimod");
}
