package ru.mineapi.modfinder;

import ru.mineapi.di.MineApiContext;
import ru.mineapi.mod.MineApiMod;
import ru.mineapi.mod.MineApiMods;
import ru.mineapi.mod.ModInfo;
import ru.mineapi.modfinder.config.ModFinderConfig;
import ru.swayfarer.swl2.asm.classfinder.ClassFinder;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.ioc.DIManager.DISwL;
import ru.swayfarer.swl2.ioc.componentscan.DISwlComponent;
import ru.swayfarer.swl2.logger.ILogger;
import ru.swayfarer.swl2.logger.LoggingManager;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.observable.IObservable;
import ru.swayfarer.swl2.observable.Observables;
import ru.swayfarer.swl2.resource.file.FileSWL;
import ru.swayfarer.swl2.string.StringUtils;

/**
 * Искалка модов <br>
 * Сканирует папку с модами и находит все файлы с расширением {@link ModFinderConfig#modFileExtension} <br>
 * В каждом фалйе ищутся классы, отмеченные аннотацией {@link MineApiMod} <br>
 * Эти классы загружаются, их Id проверяются на конфликты и полученные объекты передаются в {@link MineApiMods}
 * @author swayfarer
 *
 */
@SuppressWarnings("unchecked")
@DISwlComponent(name = "modsFinder", context = MineApiContext.contextName)
public class ModFinder {
	
	/** Событие фатальной ошибки во время загрузки. Например, дублирование Id */
	@InternalElement
	public IObservable<String> eventFatal = Observables.createObservable();
	
	/** Логировать ли события? */
	@InternalElement
	public boolean isLoggingScan = false;
	
	/** Логгер */
	@InternalElement
	public static ILogger logger = LoggingManager.getLogger();
	
	/** Файл */
	@InternalElement
	public FileSWL file;
	
	/** Искалка классов */
	@InternalElement
	public ClassFinder classFinder = new ClassFinder();
	
	/** Функция, возвращающая классы модов, расположенные в передаваемом файле */
	@InternalElement
	@DISwL
	public IFunction1<FileSWL, IExtendedList<Class<?>>> fileToModsClassesFun;
	
	/** Загрузчик модов */
	@InternalElement
	@DISwL
	public MineApiMods modsLoader;
	
	/** Директория с модами */
	@InternalElement
	@DISwL
	public FileSWL modsDir;
	
	/** Сканировать директорию модов */
	public synchronized void scan()
	{
		modsDir.getAllSubfiles((f) -> f.getName().endsWith(".mineapimod"))
		.each((f) -> {
			file = f;
			scanCurrent();
		});
	}
	
	/** Сканировать текущую директорию, заданную в {@link #scan()} */
	@InternalElement
	public void scanCurrent()
	{
		if (isLoggingScan)
			logger.info("Scanning file", file);
		
		IExtendedList<Class<?>> modClasses = getClassesForFile(file);
		
		for (Class<?> cl : modClasses)
		{
			logger.safe(() -> loadMod(cl), "Error while loading mod from", cl);
		}
	}
	
	/**
	 * Получить классы модов, лежащие в файле
	 * @param file Сканируемый файл
	 * @return Список классов 
	 */
	@InternalElement
	public IExtendedList<Class<?>> getClassesForFile(FileSWL file)
	{
		return fileToModsClassesFun.apply(file);
	}
	
	/**
	 * Задать текущий файл для сканирования через {@link #scanCurrent()} 
	 * @param file Задаваемый файл
	 * @return Этот объект (this)
	 */
	@InternalElement
	public <T extends ModFinder> T setFile(FileSWL file)
	{
		this.file = file;
		this.classFinder.useJar(file);
		return (T) this;
	}
	
	/**
	 * Загрузить мод из класса 
	 * @param modClass Класс мода
	 * @throws Throwable 
	 */
	@InternalElement
	public void loadMod(Class<?> modClass) throws Throwable
	{
		MineApiMod modAnnotation = modClass.getAnnotation(MineApiMod.class);
		
		String modId = modAnnotation.id();
		
		ModInfo alreadyExistingInfo = modsLoader.getModById(modId);
		
		if (alreadyExistingInfo != null)
		{
			String errorString = "Mod " + modId + " from '" + file.getLocalPath(modsDir) + "' has same id with mod from '" + alreadyExistingInfo.getModSource().getLocalPath(modsDir) + "'";
			
			eventFatal.next(errorString);
			logger.error(errorString);
		}
		
		if (StringUtils.isBlank(modId))
		{
			logger.error("Mod from class", modClass, "because its id is blank! Id must contains non-whitespace-chars!");
			return;
		}
		
		Object modInstance = ReflectionUtils.newInstanceOf(modClass);
		
		if (modInstance == null)
		{
			logger.error("Can't load mod from", modClass, ". Maybe constructor without args not exists? skiping...");
			return;
		}
		
		ModInfo modInfo = ModInfo.of(modInstance, modId, modAnnotation.version(), file);
		modsLoader.loadMod(modInfo);
	}
}
