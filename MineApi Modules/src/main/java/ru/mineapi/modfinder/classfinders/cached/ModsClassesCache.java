package ru.mineapi.modfinder.classfinders.cached;

import ru.mineapi.mod.MineApiMod;
import ru.mineapi.modfinder.classfinders.CachedModClassFinder;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.swconf.config.AutoSerializableConfig;

/**
 * Кэш для {@link CachedModClassFinder}, который хранит информацию о прошлых сканированиях папки модов 
 * @author swayfarer
 *
 */
public class ModsClassesCache extends AutoSerializableConfig {

	/** Список информаций о фалах */
	@InternalElement
	public IExtendedList<JarFileCache> jars = CollectionsSWL.createExtendedList();
	
	/**
	 * Получить информацию для файла, находящегося по указанному пути относительно папки модов
	 * @param str Относительный путь файла от папки модов
	 * @return Кэшированный {@link JarFileCache} или null, если не найдется
	 */
	@InternalElement
	public JarFileCache getCache(String str)
	{
		return jars.dataStream().first((e) -> e.path.equals(str));
	}
	
	/**
	 * Кэшированная информация о моде <br>
	 * Создается во время сканирования через {@link CachedModClassFinder} для каждого файла-мода <br>
	 * @author swayfarer
	 *
	 */
	@InternalElement
	public static class JarFileCache {
		
		/** Путь до файла относительно папки mods */
		@InternalElement
		public String path;
		
		/** Хэш файла в формате MD-5*/
		@InternalElement
		public String hash;
		
		/** Список имен классов ({@link Class#getName()}), отмеченных аннотацией {@link MineApiMod} */
		@InternalElement
		public IExtendedList<String> classes = CollectionsSWL.createExtendedList();
	}
}
