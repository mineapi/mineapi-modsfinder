package ru.mineapi.modfinder.classfinders;

import ru.mineapi.modfinder.classfinders.cached.ModsClassesCache;
import ru.mineapi.modfinder.classfinders.cached.ModsClassesCache.JarFileCache;
import ru.swayfarer.swl2.classes.ReflectionUtils;
import ru.swayfarer.swl2.collections.CollectionsSWL;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.ioc.DIManager.DISwL;
import ru.swayfarer.swl2.markers.InternalElement;
import ru.swayfarer.swl2.math.MathUtils;
import ru.swayfarer.swl2.resource.file.FileSWL;

/**
 * Сканнер модов, использующий кэш <br>
 * Использует {@link ScannerModsClassFinder} для поиска классов модов, но кэширует старые значения по хэшу файла <br>
 * Если данные в кэше с прошлого запуска есть информация для сканируемого файла, и она актуальна(хэши совпадают), то будет использовано кэшированное значение <br>
 * Если данных в кэше нет, или файлы устарели, кэш будет обновлен с учетом нового сканирования
 * @author swayfarer
 *
 */
public class CachedModClassFinder implements IFunction1<FileSWL, IExtendedList<Class<?>>> {

	/** Директория модов */
	@InternalElement
	@DISwL
	public FileSWL modsDir;
	
	/** Кэшированные результаты прошлых сканирований */
	@InternalElement
	@DISwL
	public ModsClassesCache modsClassesCache;
	
	/** Искалка классов модов, которая будет использована для сканирования jar, если это понадобится */
	@InternalElement
	public ScannerModsClassFinder scannerFinder = new ScannerModsClassFinder();
	
	/** 
	 * Получить кэшированные данные о jar-файле
	 * @param file Файл, для которого ищется информация
	 * @return Список классов модов или null, если информация не найдется или устареет (в этом случае она будет автоматически удалена)
	 */
	public IExtendedList<Class<?>> getCachedClasses(FileSWL file)
	{
		JarFileCache cache = modsClassesCache.getCache(file.getLocalPath(modsDir));
		
		if (!cache.hash.equals(file.getHash(MathUtils.HASH_MD5)))
		{
			modsClassesCache.jars.remove(cache);
			return null;
		}
		
		return ReflectionUtils.forceCast(
				cache.classes.dataStream()
					.mapped(ReflectionUtils::findClass)
				.toList()
			);
	}
	
	@Override
	public IExtendedList<Class<?>> apply(FileSWL file)
	{
		IExtendedList<Class<?>> classes = getCachedClasses(file);

		if (!CollectionsSWL.isNullOrEmpty(classes))
		{
			return classes;
		}

		return scannerFinder.apply(file);
	}
}
