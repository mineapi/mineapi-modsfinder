package ru.mineapi.modfinder.classfinders;

import java.util.zip.ZipFile;

import ru.mineapi.mod.MineApiMod;
import ru.swayfarer.swl2.asm.classfinder.ClassFinder;
import ru.swayfarer.swl2.collections.extended.IExtendedList;
import ru.swayfarer.swl2.functions.GeneratedFunctions.IFunction1;
import ru.swayfarer.swl2.resource.file.FileSWL;

/**
 * Искалка классов модов внутри jar-файла <br>
 * Получает на вход файл, который парсит как {@link ZipFile} и вовзращает список классов, отмеченных аннотацией {@link MineApiMod} 
 * @author swayfarer
 *
 */
public class ScannerModsClassFinder implements IFunction1<FileSWL, IExtendedList<Class<?>>> {

	@Override
	public IExtendedList<Class<?>> apply(FileSWL file)
	{
		ClassFinder classFinder = new ClassFinder();
		
		classFinder.useJar(file);
		
		return classFinder.dataStream("")
			.annotated(MineApiMod.class)
			.pulblics()
			.loadClasses()
		.toList();
	} 

}
